package podstawy.zadanie4;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class PeselTest {


    @Test
    @DisplayName("Sprawdzam czy ciąg jest poprawnym PESELem")
    void czyJestPeselem() {
        String name = "34052591308";
        boolean expected = true;
        boolean actual = Pesel.czyPoprawnyPesel(name);
        assertThat(actual).isEqualTo(expected);

    }

    @Test
    @DisplayName("Sprawdzam czy ciąg NIE jest poprawnym PESELem")
    void czyNieJestPeselem() {
        String name = "340525913";
        boolean expected = false;
        boolean actual = Pesel.czyPoprawnyPesel(name);
        assertThat(actual).isEqualTo(expected);
    }
}