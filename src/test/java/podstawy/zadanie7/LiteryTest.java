package podstawy.zadanie7;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

public class LiteryTest {

    @Test
    @DisplayName("Sprawdzam konkatenacje pierwszy liter wyrazów")
    void getFirstCharTest1() {
        //given
        String tekst = "ala ma kota";
        String expected ="amk";
        //when
        String actual = Litery.getFirstChar(tekst);
        //then
        assertEquals(expected,actual);
        assertThat(actual)
                .isEqualTo(expected)
                .isNotNull()
                .isNotEmpty();
    }
    @Test
    @DisplayName("Sprawdzam konkatenacje pierwszy liter wyrazów")
    void getFirstCharTest2() {
        //given
        String tekst = "Ala ma KOta";
        String expected ="AmK";
        //when
        String actual = Litery.getFirstChar(tekst);
        //then
        assertEquals(expected,actual);
        assertThat(actual)
                .isEqualTo(expected)
                .isNotNull()
                .isNotEmpty();
    }

    @Test
    @DisplayName("Sprawdzam konkatenacje pierwszy liter wyrazów")
    void getFirstCharTest3() {
        //given
        String tekst = "ALa";
        String expected ="A";
        //when
        String actual = Litery.getFirstChar(tekst);
        //then
        assertEquals(expected,actual);
        assertThat(actual)
                .isEqualTo(expected)
                .isNotNull()
                .isNotEmpty();
    }
    @Test
    @DisplayName("Sprawdzam konkatenacje pierwszy liter wyrazów")
    void getFirstCharTest4() {
        //given
        String tekst = " ";
        String expected ="";
        //when
        String actual = Litery.getFirstChar(tekst);
        //then
        assertEquals(expected,actual);
        assertThat(actual)
                .isEqualTo(expected)
                .isNotNull()
                .isEmpty();

    }
}