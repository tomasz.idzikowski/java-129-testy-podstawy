package podstawy.zadanie5;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DuplikatyTest {
    @Test
    void shouldListContainDuplicates() {
        List<String> expected = Arrays.asList("raz", "dwa", "trzy");
        List<String> actual = Duplikaty.removeDuplicates(Arrays.asList("raz", "dwa", "trzy", "raz", "dwa"));
        assertThat(actual)
                .isNotNull()
                .isNotEmpty()
                .isNotSameAs(expected)
                .isEqualTo(expected)
                .containsExactlyInAnyOrder("raz", "dwa", "trzy")
                .hasSize(3);

    }
}