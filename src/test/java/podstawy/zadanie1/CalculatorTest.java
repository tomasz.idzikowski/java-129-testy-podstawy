package podstawy.zadanie1;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

class CalculatorTest {

    private Calculator calculator;

    @Test
    @DisplayName("Should add two positive numbers")
    void add() {
        double firstNumber = 5;
        double secondNumber = 10;
        double expected = 15;
        double actual = calculator.add(firstNumber, secondNumber);
        assertEquals(expected,actual);           //junit
        assertThat(actual).isEqualTo(expected); //assertj
    }

    @Test
    @DisplayName("Should add two negative numbers")
    void add2() {
        double firstNumber = -5;
        double secondNumber = -10;
        double expected = -15;
        double actual = calculator.add(firstNumber, secondNumber);
        assertEquals(expected,actual);           //junit
        assertThat(actual).isEqualTo(expected); //assertj
    }

    @Test
    @DisplayName("Should add two 0")
    void add3() {
        double firstNumber = 0;
        double secondNumber = 0;
        double expected = 0;
        double actual = calculator.add(firstNumber, secondNumber);
        assertEquals(expected,actual);           //junit
        assertThat(actual).isEqualTo(expected); //assertj
    }

    @BeforeEach
    void setUp() {
        System.out.println("Uruchamianie testu");
        calculator=new Calculator();
    }

    @AfterEach
    void tearDown() {
        System.out.println("Test został zakonczony");
    }

    @BeforeAll
    static void before(){
        System.out.println("Przed uruchomieniem klasy/przed wszystkimi metodami");
    }

    @AfterAll
    static void after(){
        System.out.println("Po wszystkich testach");
    }
}