package podstawy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class TabliceTest {
    @Test
    void testujTablice() {
        int[] expected = {1, 2, 3};
        int[] actual = {1, 2, 3};
        assertArrayEquals(expected, actual);
        assertNotNull(actual);             // junit5
        assertThat(actual)
                .isNotNull()
                .isNotEmpty()
                .isEqualTo(expected);       //assertJ
    }

    @Test
    void testujListy() {
        List<Integer> expected = Arrays.asList(1, 2, 3); //niemodyfikowalna
        List<Integer> actual = Arrays.asList(1, 2, 3); //niemodyfikowalna
        assertIterableEquals(expected, actual); //junit
        assertNotNull(actual);
        assertThat(actual)
                .isNotNull()
                .isNotEmpty()
                .hasSize(3)
                .isEqualTo(expected);       //assertJ


        assertAll(
                () -> assertThat(actual).isNotNull(),
                () -> assertThat(actual).isNotEmpty(),
                () -> assertThat(actual).hasSize(3),
                () -> assertThat(actual).isEqualTo(expected)
        );
    }
}
