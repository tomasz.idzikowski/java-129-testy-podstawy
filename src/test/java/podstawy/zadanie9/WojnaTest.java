package podstawy.zadanie9;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class WojnaTest {

    @Test
    void play() {
        String player1 = "Player1";
        String player2 = "CPU";
        List<KartaDoGry> cards1 = new ArrayList<>(Arrays.asList(
                new KartaDoGry(Figura.DAMA, Kolor.PIK),
                new KartaDoGry(Figura.AS, Kolor.KARO)));

        List<KartaDoGry> cards2 = new ArrayList<>(Arrays.asList(
                new KartaDoGry(Figura.DAMA, Kolor.TREFL),
                new KartaDoGry(Figura.KROL, Kolor.KARO),
                new KartaDoGry(Figura.AS,Kolor.KIER)));
        String actualWinner = Wojna.play(player1, player2, cards1, cards2);

        assertThat(actualWinner).isEqualTo(player1);
    }
}