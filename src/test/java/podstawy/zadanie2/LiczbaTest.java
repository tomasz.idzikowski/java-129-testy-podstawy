package podstawy.zadanie2;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

class LiczbaTest {

    @Test
    @DisplayName("Czy jest pierwsza")
    void czyPierwsza() {
        int number = 7;
        boolean actual = Liczba.czyPierwsza(number);
        assertTrue(actual); //junit
        assertThat(actual).isTrue(); //assertJ
    }

    @Test
    @DisplayName("Czy jest zlozona")
    void czyZlozona() {
        int number = 14;
        boolean actual = Liczba.czyPierwsza(number);
        assertFalse(actual); //junit
        assertThat(actual).isFalse(); //assertJ
    }
}