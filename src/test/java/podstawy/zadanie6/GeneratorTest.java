package podstawy.zadanie6;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class GeneratorTest {
    @Test
    void losowanie() {
        int a = 1;
        int b = 49;
        int x = 6;
        Set<Integer> actual = Generator.generateNumbers(x, a, b);
        assertThat(actual)
                .hasSize(x)
                .filteredOn(liczba -> liczba >= a && liczba <= b);

    }

    @Test
    void losowanie2() {
        int a = 7;
        int b = 10;
        int x = 4;
        Set<Integer> actual = Generator.generateNumbers(x, a, b);
        assertThat(actual)
                .hasSize(x)
                .allSatisfy(liczba ->
                        assertThat(liczba)
                                .isGreaterThanOrEqualTo(a)
                                .isLessThanOrEqualTo(b));

    }
}
