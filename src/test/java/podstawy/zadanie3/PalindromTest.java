package podstawy.zadanie3;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import podstawy.zadanie2.Liczba;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PalindromTest {

    @Test
    @DisplayName("Czy słowo jest palindromem")
    void shoulBePalindrom() {
        String text = "kajak";
        boolean actual = Palindrom.czyPalindrom(text);
        assertTrue(actual); //junit
        assertThat(actual).isTrue(); //assertJ
    }

    @Test
    @DisplayName("Czy słowo nie jest palindromem")
    void shouldNotBePalindrom() {
        String text = "komputer";
        boolean actual = Palindrom.czyPalindrom(text);
        assertFalse(actual); //junit
        assertThat(actual).isFalse(); //assertJ
    }


    @Test
    @DisplayName("Czy słowo nie jest palindromem")
    void shouldBePalindrom() {
        String text = "kAJak";
        boolean actual = Palindrom.czyPalindrom(text);
        assertTrue(actual); //junit
        assertThat(actual).isTrue(); //assertJ
    }
}