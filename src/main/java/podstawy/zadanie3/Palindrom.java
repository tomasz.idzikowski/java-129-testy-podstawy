package podstawy.zadanie3;

public class Palindrom {
    public static boolean czyPalindrom(String text) {
        String reversed = new StringBuilder(text.toLowerCase()).reverse().toString();
        if (reversed.equals(text.toLowerCase())){
            return true;
        }
        return false;
    }
}
